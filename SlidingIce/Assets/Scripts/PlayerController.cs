﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private float playerSpeed;

	[SerializeField]
	private float m_resetHeight = -5.0f;

	[SerializeField]
	private float m_friction = 0.2f;

	[SerializeField]
	private Transform m_mesh;

	[SerializeField]
	private float m_exitSpinSpeed = 2000f;

	[SerializeField]
	private float m_exitFlyUpSpeed = 20.0f;

    private Vector3 m_moveDirection;
    private bool isMoving;
    private Transform m_transform;
	private bool m_canMove = true;
	private int m_exitState = 0;
	private Vector3 m_exitPosition;
    private Rigidbody m_rigidbody;
	public static int m_moves = 0;

	private bool hitPolarBear;
	private float polarBearTimer;

	[HideInInspector]
	public int moveTracker;

	private enum MoveDir
	{
		Reset = 0,
		Up = 1,
		Down = 2,
		Left = 3,
		Right = 4
	}
	private MoveDir m_lastMoveDirecton = MoveDir.Reset;

    private void Start()
    {
        m_transform = transform;
        m_rigidbody = GetComponent<Rigidbody>();
        StopMoving();
		//m_moves = 0;
		m_lastMoveDirecton = MoveDir.Reset;
    }

    private void FixedUpdate()
    {
		if (m_exitState > 0)
		{
			ExitUpdate ();
			return;
		}

        if (!isMoving)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                MoveUp();
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                MoveDown();
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                MoveRight();
            }
        }
        else
        {
            Move();
        }

		// Restart if fallen off level
		if (m_transform.position.y < m_resetHeight)
		{
			GameManager.Instance.RestartLevel();
		}

		if (hitPolarBear) {
			polarBearTimer += Time.deltaTime;
		}

		if (polarBearTimer >= 0.1f) {
			StopMoving ();
			hitPolarBear = false;
			polarBearTimer = 0;
		}

    }

	private void ExitUpdate()
	{
		switch (m_exitState)
		{
		case 1:
			Vector3 distToPos = m_exitPosition - m_transform.position;
			m_rigidbody.MovePosition (m_transform.position + distToPos * Time.deltaTime * playerSpeed);
			if (distToPos.sqrMagnitude < 0.1f)
			{
				StopMoving ();
				m_exitState++;
			}
			break;
		case 2:
			m_rigidbody.useGravity = false;
			m_rigidbody.velocity = new Vector3 (0, m_exitFlyUpSpeed, 0);
			m_transform.RotateAround (m_transform.position, Vector3.up, m_exitSpinSpeed * Time.deltaTime);
			break;
		}
	}

    private void Move()
	{
		if (playerSpeed > 0.0f && !IsGrounded())
        {
			//playerSpeed -= m_friction;
		}
		m_rigidbody.MovePosition(m_transform.position + m_moveDirection * Time.deltaTime * playerSpeed);
    }

    public void MoveUp()
    {
		if (!isMoving/* && m_lastMoveDirecton != MoveDir.Up*/)
        {
            m_moveDirection = Vector3.forward;
            isMoving = true;
            m_mesh.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
			m_lastMoveDirecton = MoveDir.Up;
			m_moves++;
        }
    }

    public void MoveDown()
    {
		if (!isMoving/* && m_lastMoveDirecton != MoveDir.Down*/)
        {
            m_moveDirection = Vector3.back;
            isMoving = true;
            m_mesh.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
			m_lastMoveDirecton = MoveDir.Down;
			m_moves++;
        }
    }

    public void MoveLeft()
    {
		if (!isMoving/* && m_lastMoveDirecton != MoveDir.Left*/)
        {
            m_moveDirection = Vector3.left;
            isMoving = true;
            m_mesh.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
			m_lastMoveDirecton = MoveDir.Left;
			m_moves++;
        }
    }

    public void MoveRight()
    {
		if (!isMoving/* && m_lastMoveDirecton != MoveDir.Right*/)
        {
            m_moveDirection = Vector3.right;
            isMoving = true;
            m_mesh.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
			m_lastMoveDirecton = MoveDir.Right;
			m_moves++;
        }
    }

    private void StopMoving()
    {
        isMoving = false;

        m_rigidbody.velocity = Vector3.zero;

        // Lock the position to the grid
        Vector3 pos = m_transform.position;
        m_transform.position = new Vector3(Mathf.RoundToInt(pos.x), 0, Mathf.RoundToInt(pos.z));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isMoving && collision.collider.tag == "Obstacle")
        {
            StopMoving();
        }

		if (isMoving && collision.collider.tag == "Polar Bear") {

			StopMoving ();

			KnockBack ();
		}

		if (isMoving && collision.collider.tag == "Polar Bear Back") {

			StopMoving ();

			hitPolarBear = true;

			KnockBack ();
		}

    }

    private bool IsGrounded()
    {
       return Physics.Raycast(transform.position + Vector3.up*2.0f, -Vector3.up, 5.0f);
    }

	public void StartExitSpin(Vector3 exitPos)
	{
		moveTracker = m_moves;
		PlayerPrefs.SetInt(GameManager.Instance.CurrentLevel.ToString(), m_moves);

		print (moveTracker);
		print (m_moves);

		m_moves = 0;

		m_exitState = 1;
		isMoving = false;
		m_exitPosition = exitPos;

		if (m_moves <= GameManager.Instance.GetCurrentLevelData ().threeStars) {
			Debug.print ("Three Stars");
		} else if (m_moves <= GameManager.Instance.GetCurrentLevelData ().twoStars) {
			Debug.print ("Two Stars");
		} else if (m_moves <= GameManager.Instance.GetCurrentLevelData ().oneStar) {
			Debug.print ("One Star");
		} else {
			Debug.print ("Zero Stars");
		}
			
	}

	void KnockBack(){
		if (m_lastMoveDirecton == MoveDir.Right) {
			MoveLeft ();
		} else if (m_lastMoveDirecton == MoveDir.Left){
			MoveRight ();
		} else if (m_lastMoveDirecton == MoveDir.Up){
			MoveDown ();
		} else if (m_lastMoveDirecton == MoveDir.Down){
			MoveUp ();
		}

		m_moves--;
	}
}
