﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetNumberOfMoves : MonoBehaviour {

	private PlayerController playerController;
	public Text moveNumber;

	// Use this for initialization
	void Start () {
		playerController = GameObject.FindObjectOfType<PlayerController> ();
		moveNumber.text = "" + playerController.moveTracker;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
