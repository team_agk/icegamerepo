﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeInput : MonoBehaviour
{
    [SerializeField]
    private PlayerController m_playerController = null;

	[SerializeField]
	private float m_dragScreenHeightPercent = 0.10f;

    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered
    private bool m_reset = true;

    void Start()
    {
		dragDistance = Screen.height * m_dragScreenHeightPercent; //dragDistance is 15% height of the screen
    }

    void Update()
    {
        if (m_playerController == null)
        {
            m_playerController = gameObject.GetComponent<PlayerController>();
            return;
        }

        if (Input.touchCount == 1) // user is touching the screen with a single touch
        {
            Touch touch = Input.GetTouch(0); // get the touch
            if (touch.phase == TouchPhase.Began) //check for the first touch
            {
                fp = touch.position;
                lp = touch.position;
                m_reset = true;
            }
            else if (touch.phase == TouchPhase.Moved && m_reset) // update the last position based on where they moved
            {
                lp = touch.position;
            //}
            //else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
            //{
            //    lp = touch.position;  //last touch position. Ommitted if you use list

                //Check if drag distance is greater than 20% of the screen height
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {//It's a drag
                 //check if the drag is vertical or horizontal
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                    {   //If the horizontal movement is greater than the vertical movement...
                        if ((lp.x > fp.x))  //If the movement was to the right)
                        {   //Right swipe
                            Debug.print("Right Swipe");
                            m_playerController.MoveRight();
                            m_reset = false;
                        }
                        else
                        {   //Left swipe
                            Debug.print("Left Swipe");
                            m_playerController.MoveLeft();
                            m_reset = false;
                        }
                    }
                    else
                    {   //the vertical movement is greater than the horizontal movement
                        if (lp.y > fp.y)  //If the movement was up
                        {   //Up swipe
                            Debug.print("Up Swipe");
                            m_playerController.MoveUp();
                            m_reset = false;
                        }
                        else
                        {   //Down swipe
                            Debug.print("Down Swipe");
                            m_playerController.MoveDown();
                            m_reset = false;
                        }
                    }
                }
                else
                {   //It's a tap as the drag distance is less than 20% of the screen height
                    Debug.print("Tap");
                }
            }
        }
    }
}
