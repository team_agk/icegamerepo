﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gibs : MonoBehaviour
{
    private Transform m_transform;

    [SerializeField]
    private float m_destroyHeight = -5.0f;

    void Start()
    {
        m_transform = transform;
        enabled = false;
    }

	void Update ()
    {
        if (m_transform.position.y < m_destroyHeight)
        {
            Destroy(this.gameObject);
        }
    }
}
