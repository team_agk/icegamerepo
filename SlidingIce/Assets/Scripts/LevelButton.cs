﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour {

	[SerializeField]
	private Text myText;

	[SerializeField]
	private LevelSelectControl buttonListControl;

	[SerializeField]
	private MainMenuController mainMenuController;

	private string myTextString;

	private int levelID;

	public string levelString;

	[HideInInspector]
	public static int levelTracker;

	const string selectedLevel = "selectedLevel";

	void Start()
	{
		levelTracker++;

		levelID = levelTracker;

		levelString = "Puzzle" + levelID;
	}

	public void SetText(string textString)
	{
		myTextString = textString;
		myText.text = textString;
	}

	public void SetLevelToLoad()
	{
		SceneManager.LoadScene (levelString);
	}

	public void SetLevel()
	{
		PlayerPrefs.SetString (selectedLevel, levelString);

		mainMenuController.LoadFromLevelSelect ();
	}

	public void ResetLevelTracker()
	{
		levelTracker = 0;
	}
		
}
