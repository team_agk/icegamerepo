﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShatterBlock : MonoBehaviour {

	private bool shouldShatter;

	private float timer;
	[SerializeField]
	private float timeToShatter;

    private List<Rigidbody> m_gibs = new List<Rigidbody>();

    [SerializeField]
    protected float m_destructionForce = 20f;
    [SerializeField]
    protected float m_destructionForceRadius = 5f;

    void Start()
	{
		shouldShatter = false;

        Rigidbody[] rigidBodies = GetComponentsInChildren<Rigidbody>();
        for (int i = 0; i < rigidBodies.Length; i++)
        {
            Rigidbody item = rigidBodies[i];

            if (item.gameObject != this.gameObject)
            {
                item.useGravity = false;
                item.isKinematic = true;
                m_gibs.Add(item);
            }
        }
	}

	void Update ()
	{
		if (shouldShatter)
		{
			timer += Time.deltaTime;
		}

		if (timer >= timeToShatter)
		{
			Destroy (this.gameObject);
		}
	}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            Gib(transform.position);
            StartTimer();
        }
    }

    /*void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerFace")
            StartTimer();
    }*/

	void StartTimer()
	{
		shouldShatter = true;
	}

    void Gib(Vector3 forcePosition)
    {
        for (int i = 0; i < m_gibs.Count; i++)
        {
            Rigidbody item = m_gibs[i];

            item.transform.parent = null;
            item.useGravity = true;
            item.isKinematic = false;

            item.AddExplosionForce(m_destructionForce, forcePosition, m_destructionForceRadius, 1f, ForceMode.Impulse);
            item.AddTorque(new Vector3(UnityEngine.Random.Range(-1.0f, 1.0f), UnityEngine.Random.Range(-1.0f, 1.0f), UnityEngine.Random.Range(-1.0f, 1.0f)) * m_destructionForce, ForceMode.Impulse);

            item.GetComponent<Gibs>().enabled = true;
        }
    }
}

