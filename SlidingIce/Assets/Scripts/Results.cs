﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Results : MonoBehaviour
{

	private bool m_resultsActive = false;

	void Start ()
	{
		m_resultsActive = false;
	}

	void Update ()
	{
		if (m_resultsActive && Input.anyKeyDown)
		{
			GameManager.Instance.LoadNextLevel();
		}
	}

	public void ShowResults()
	{
		m_resultsActive = true;
		transform.GetChild(0).gameObject.SetActive(true);
	}

}
