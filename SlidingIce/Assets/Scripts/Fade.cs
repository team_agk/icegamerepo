﻿using System;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public static class ExtensionMethods
{
    public static void SetAlpha(this Material material, float value)
    {
        Color color = material.color;
        color.a = value;
        material.color = color;
    }
}

class Fade : MonoBehaviour
{
    private Material m_material;
    private Renderer m_renderer = null;
    private float m_fraction = 1.0f;
    private bool m_fade = false;

    private void Start()
    {
        m_renderer = GetComponent<Renderer>();
        m_renderer.enabled = true;
        m_material = m_renderer.sharedMaterial;

        m_material.SetAlpha(1.0f);

        m_fraction = 1.0f;
        m_fade = false;
    }

    public void Update()
    {
        if (!m_fade)
        {
            m_fraction -= Time.deltaTime;
            m_fraction = Math.Max(0.0f, m_fraction);

            if (m_fraction <= 0.0f)
            {
                m_renderer.enabled = false;
                return;
            }

            m_material.SetAlpha(m_fraction);
        }
    }
}
