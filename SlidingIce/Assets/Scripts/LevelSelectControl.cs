﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectControl : MonoBehaviour {

	[SerializeField]
	private GameObject buttonTemplate;

	private List<GameObject> buttons;

	public bool islevelList;

	public LevelSelectTracker levelSelectedTracker;

	void Start()
	{
		GenerateList ();
	}

	void GenerateList()
	{
		buttons = new List<GameObject> ();

		if (buttons.Count > 0)
		{
			foreach (GameObject button in buttons)
			{
				Destroy (button.gameObject);
			}
			buttons.Clear();
		}

		if (!islevelList) {
			for (int i = 1;  i < levelSelectedTracker.numOfStages +1; i++) {
				GameObject button = Instantiate (buttonTemplate) as GameObject;
				button.SetActive (true);

				button.GetComponent<StageButton> ().SetText ("Stage " + i);

				button.transform.SetParent (buttonTemplate.transform.parent, false); //false so button doesn't position itself in World Space
			}
		} else {
			for (int i = 1; i < levelSelectedTracker.levelsPerStage +1; i++) {
				GameObject button = Instantiate (buttonTemplate) as GameObject;
				button.SetActive (true);

				button.GetComponent<LevelButton> ().SetText ("Level " + i);

				button.transform.SetParent (buttonTemplate.transform.parent, false); //false so button doesn't position itself in World Space
			}
		}
	}

	public void OpenLevelMenu()
	{
		transform.GetChild (03).gameObject.SetActive (true);
	}

	public void CloseLevelMenu()
	{
		transform.GetChild (03).gameObject.SetActive (false);
	}

}
