﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageButton : MonoBehaviour {

	[SerializeField]
	private Text myText;

	[SerializeField]
	private LevelSelectControl buttonListControl;

	[SerializeField]
	private MainMenuController mainMenuController;

	[SerializeField]
	private string levelName;

	private string myTextString;

	private int stageID;

	private static int stageTracker;

	private string stageString;

	const string selectedStage = "selectedStage";

	void Start()
	{
		stageTracker++;

		stageID = stageTracker;

		stageString = "Stage" + stageID;
	}

	public void SetText(string textString)
	{
		myTextString = textString;
		myText.text = textString;
	}

	public void SetStage()
	{
		PlayerPrefs.SetString (selectedStage, stageString);
	}

	public void ResetStageTracker()
	{
		stageTracker = 0;
	}
}
