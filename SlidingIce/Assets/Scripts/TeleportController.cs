﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportController : MonoBehaviour {

	[HideInInspector]
	public bool canTeleport;

	private float timeToTeleport;

	[SerializeField]
	private PlayerController playerController;

	// Use this for initialization
	void Start () {
		canTeleport = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!canTeleport)
			timeToTeleport += Time.deltaTime;

		if (timeToTeleport >= 0.1f)
		{
			canTeleport = true;
			timeToTeleport = 0;
		}
	}

	public void Teleport(Vector3 target)
	{
		canTeleport = false;
		playerController.transform.position = target;
	}
}
