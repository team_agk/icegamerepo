﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct LevelData
{
	public string name;
	public string artScene;
	public int oneStar;
	public int twoStars;
	public int threeStars;
}

public class GameLevels : MonoBehaviour
{
   
	[SerializeField]
	public List<LevelData> m_levelData;

	public List<LevelData> levels { get { return m_levelData; } }
}