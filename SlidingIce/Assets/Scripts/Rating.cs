﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rating : MonoBehaviour {

	public Sprite[] numOfStars;
	private PlayerController playerController;
	Image imageComponent;

	// Use this for initialization
	void Start () {
		playerController = GameObject.FindObjectOfType<PlayerController> ();
		imageComponent = GetComponent<Image> ();
		SetStarsSprite ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SetStarsSprite(){
		if (playerController.moveTracker <= GameManager.Instance.GetCurrentLevelData ().threeStars) {
			imageComponent.sprite = numOfStars[2];
		} else if (playerController.moveTracker <= GameManager.Instance.GetCurrentLevelData ().twoStars) {
			imageComponent.sprite = numOfStars[1];
		} else if (playerController.moveTracker <= GameManager.Instance.GetCurrentLevelData ().oneStar) {
			imageComponent.sprite = numOfStars[0];
		} else {
			Debug.print ("Zero Stars");
		}
	}
}
