﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour
{
    private List<CollectSnowflake> m_snowFlakes = new List<CollectSnowflake>();
    private MeshRenderer m_renderer;
	private bool m_finiedLevel = false;

	private Results results;

    private void Start()
    {
        CollectSnowflake[] snowflakes = FindObjectsOfType<CollectSnowflake>();
        for (int i = 0; i < snowflakes.Length; i++)
        {
            m_snowFlakes.Add(snowflakes[i]);
        }

        m_renderer = GetComponent<MeshRenderer>();
		m_renderer.material.color = Color.black;
        //m_renderer.enabled = false;

		results = GameObject.FindObjectOfType<Results>();
    }

	void OnTriggerEnter (Collider other)
	{
		if (!m_finiedLevel && other.tag == "Player" && m_snowFlakes.Count == 0)
	    {
	        //GameManager.Instance.LoadNextLevel();

			PlayerController player = other.GetComponent<PlayerController>();
			if (player != null)
			{
				player.StartExitSpin (transform.position);
			}
			PlayerPrefs.SetInt(GameManager.Instance.CurrentLevel.ToString(), 1);
			StartCoroutine(NextLevel());
			m_finiedLevel = true;
	    }
	}

    public void SnowflakeCollected(CollectSnowflake snowflake)
    {
        m_snowFlakes.Remove(snowflake);
        if (m_snowFlakes.Count == 0)
        {
            //m_renderer.enabled = true;
			m_renderer.material.color = Color.yellow;
        }
    }

	IEnumerator NextLevel()
	{
		yield return new WaitForSeconds (1.0f);
		results.ShowResults ();
		//GameManager.Instance.LoadNextLevel();
	}
}
