﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotator : MonoBehaviour
{

	void Start ()
	{
	    MeshRenderer[] renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
	    for (int i = 0; i < renderers.Length; i++)
	    {
	        if (renderers[i].gameObject != this.gameObject)
	        {
	            renderers[i].transform.Rotate(Vector3.forward * Random.Range(0, 360));
	        }
	    }
	}
}
