﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidifyBlock : MonoBehaviour {

	private Renderer rend;

	private float timer;
	[SerializeField]
	private float timeToFreeze;

	[SerializeField]
	private Animator m_animator;

	[SerializeField]
	private Transform m_solid;

	[SerializeField]
	private Transform m_puddle;

	private int m_state = 0;
	private bool m_animated = false;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
		m_state = 0;
		m_solid.gameObject.SetActive(false);
		m_animator.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {

		if (m_state == 1)
        {
            timer += Time.deltaTime;


			if (timer >= timeToFreeze)
			{
				m_state = 2;
				FreezeBlock ();
			}
        }
	}

	void OnTriggerEnter(Collider collider)
	{
		
		if (!m_animated && collider.tag == "Player")
		{
			m_animated = true;
			m_animator.gameObject.SetActive (true);
			m_animator.SetTrigger("Freeze");
			m_puddle.gameObject.SetActive(false);
		}


	}

	void OnTriggerExit()
	{
		if (m_state == 0)
		{
			m_state = 1;
		}
	}

	void FreezeBlock()
	{
		m_solid.gameObject.SetActive(true);
	}
}
