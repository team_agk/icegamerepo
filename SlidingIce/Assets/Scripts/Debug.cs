﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug : MonoBehaviour
{
	void Update ()
    {
        if (Input.GetKey(KeyCode.Tab))
        {
            GameManager.Instance.LoadNextLevel();
        }

        if (Input.GetKey(KeyCode.R))
        {
            GameManager.Instance.RestartLevel();
        }
    }
}
