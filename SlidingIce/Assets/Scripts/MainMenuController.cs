﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

	Image imageComponent;

	public Sprite[] frontEndImage;

	int imageCounter;

	public string levelToLoad;

	// Use this for initialization
	void Start () {
		imageComponent = GetComponent<Image> ();
		//imageCounter = 0;

	}

	public void LoadNextScene(string scene)
	{
		SceneManager.LoadScene (scene);
	}

	public void LoadFromLevelSelect ()
	{
		string stage = (PlayerPrefs.GetString ("selectedStage"));
		string level = (PlayerPrefs.GetString ("selectedLevel"));

		string specifiedLevel = stage + level;

		print ("Load Scene: " + specifiedLevel);

		SceneManager.LoadScene (specifiedLevel);
		SceneManager.LoadScene("PuzzleArtScene", LoadSceneMode.Additive);
	}

	public void ChangeImage(){
		imageCounter++;

		if (imageCounter > frontEndImage.Length - 1) {
			imageCounter = 0;
		}

		imageComponent.sprite = frontEndImage[imageCounter];
	}

	public void OpenChildMenu(){
		transform.GetChild(0).gameObject.SetActive(true);
	}

	public void CloseChildMenu(){
		transform.GetChild(0).gameObject.SetActive(false);
	}

	public void LoadFirstLevel()
	{
		GameManager.Instance.LoadFirstLevel ();
	}
}
