﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance = null;

    private GameLevels m_gameLevels = null;
    private int m_currentLevel = 0;

	[SerializeField]
	private string m_mainMenu;

	public int CurrentLevel { get { return m_currentLevel; } }

	public LevelData GetCurrentLevelData()
	{
		return m_gameLevels.levels [m_currentLevel];
	}

	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy(gameObject);
	}

    void Start()
    {
        DontDestroyOnLoad(gameObject);

        m_gameLevels = GetComponent<GameLevels>();

        InitGame();
    }

    void InitGame()
    {
		LoadMainMenu();
    }

    void LoadLevel(int level)
    {
        level = Mathf.Min(level, m_gameLevels.levels.Count-1);
		LevelData levelData = m_gameLevels.levels [level];
		SceneManager.LoadScene(levelData.name);
		SceneManager.LoadScene(levelData.artScene, LoadSceneMode.Additive);
    }

	public void LoadFirstLevel()
	{
		m_currentLevel = 0;
		LoadLevel (m_currentLevel);
	}

    public void LoadNextLevel()
    {
        m_currentLevel++;
        if (m_currentLevel >= m_gameLevels.levels.Count)
        {
            m_currentLevel = 0;
        }
        LoadLevel(m_currentLevel);
    }

    public void RestartLevel()
    {
        LoadLevel(m_currentLevel);
    }

	public void LoadMainMenu()
	{
		SceneManager.LoadScene(m_mainMenu);
	}
}