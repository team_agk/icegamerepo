﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDirection : MonoBehaviour {

	private bool isMoving;

	// Use this for initialization
	void Start () {
		
		isMoving = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown(KeyCode.UpArrow) && !isMoving)
		{
			TurnUp();
			//isMoving = true;
		}

		if (Input.GetKeyDown(KeyCode.DownArrow) && !isMoving)
		{
			TurnDown();
			//isMoving = true;
		}

		if (Input.GetKeyDown(KeyCode.LeftArrow) && !isMoving)
		{
			TurnLeft();
			//isMoving = true;
		}

		if (Input.GetKeyDown(KeyCode.RightArrow) && !isMoving)
		{
			TurnRight();
			//isMoving = true;
		}

	}

	void TurnUp()
	{
		transform.localPosition = new Vector3 (0, 0, 0.25f);
		isMoving = false;
	}

	void TurnDown()
	{
		transform.localPosition = new Vector3 (0, 0, -0.25f);
		isMoving = false;
	}

	void TurnLeft()
	{
		transform.localPosition = new Vector3 (-0.25f, 0, 0);
		isMoving = false;
	}

	void TurnRight()
	{
		transform.localPosition = new Vector3 (0.25f, 0, 0);
		isMoving = false;
	}
}
