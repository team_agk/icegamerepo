﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowflakeSpinner : MonoBehaviour {

	[SerializeField]
	private float rotationSpeed;

	void Update () {

		transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
	}
}
