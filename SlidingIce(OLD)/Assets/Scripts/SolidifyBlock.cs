﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidifyBlock : MonoBehaviour {

	private Renderer rend;

	private float timer;
	[SerializeField]
	private float timeToFreeze;

	private bool startTimer;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
		startTimer = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (startTimer)
			timer += Time.deltaTime;

		if (timer >= timeToFreeze)
			FreezeBlock ();
		
	}

	void OnTriggerExit()
	{
		startTimer = true;
		//rend.material.SetColor ("_SpecColor", Color.yellow);
	}

	void FreezeBlock()
	{
		transform.GetChild(0).gameObject.SetActive(true);
	}
}
