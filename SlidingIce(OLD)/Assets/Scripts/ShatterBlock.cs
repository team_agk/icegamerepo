﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShatterBlock : MonoBehaviour {

	private bool shouldShatter;

	private float timer;
	[SerializeField]
	private float timeToShatter;

	void Start()
	{
		shouldShatter = false;
	}

	void Update ()
	{
		if (shouldShatter)
		{
			timer += Time.deltaTime;
		}

		if (timer >= timeToShatter)
		{
			Destroy (this.gameObject);
		}
	}

	/*void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.tag == "Player")
		StartTimer();
	}*/

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "PlayerFace")
			StartTimer();
	}

	void StartTimer()
	{
		shouldShatter = true;
	}
}

