﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoundaryReset : MonoBehaviour {

	[SerializeField]
	private string thisLevelName;

	void OnTriggerEnter ()
	{
		SceneManager.LoadScene (thisLevelName);
	}
}
