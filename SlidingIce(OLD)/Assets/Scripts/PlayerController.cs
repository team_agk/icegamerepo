﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[SerializeField]
	private float playerSpeed;

	private float playerSpeedUp;
	private float playerSpeedRight;
	private float playerSpeedDown;
	private float playerSpeedLeft;

	private bool isMoving;

	void Start () {

		isMoving = false;
	}

	void Update () {

		if (Input.GetKey(KeyCode.UpArrow) && !isMoving)
		{
			MoveUp();
			isMoving = true;
		}

		if (Input.GetKey(KeyCode.DownArrow) && !isMoving)
		{
			MoveDown();
			isMoving = true;
		}

		if (Input.GetKey(KeyCode.LeftArrow) && !isMoving)
		{
			MoveLeft();
			isMoving = true;
		}

		if (Input.GetKey(KeyCode.RightArrow) && !isMoving)
		{
			MoveRight();
			isMoving = true;
		}

		transform.Translate(Vector3.forward * Time.deltaTime * playerSpeedUp);
		transform.Translate(-Vector3.forward * Time.deltaTime * playerSpeedDown);
		transform.Translate(-Vector3.right * Time.deltaTime * playerSpeedLeft);
		transform.Translate(Vector3.right * Time.deltaTime * playerSpeedRight);

	}

	void MoveUp()
	{
		playerSpeedUp = playerSpeed;
	}

	void MoveDown()
	{
		playerSpeedDown = playerSpeed;
	}

	void MoveLeft()
	{
		playerSpeedLeft = playerSpeed;
	}

	void MoveRight()
	{
		playerSpeedRight = playerSpeed;
	}

	void StopMoving()
	{
		playerSpeedUp = 0;
		playerSpeedDown = 0;
		playerSpeedLeft = 0;
		playerSpeedRight = 0;
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.tag == "Player")
		{
			StopMoving();
			isMoving = false;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Obstacle")
		{
			StopMoving();
			isMoving = false;
		}
	}
}
