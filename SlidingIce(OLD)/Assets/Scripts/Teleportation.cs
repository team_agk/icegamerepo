﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleportation : MonoBehaviour {

	[SerializeField]
	private Transform target;
	private Vector3 targetVector;

	[SerializeField]
	private TeleportController teleportController;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player" && teleportController.canTeleport == true)
		{
			targetVector = new Vector3(target.position.x, target.position.y, target.position.z);
			teleportController.Teleport(targetVector);
		}
	}
}
