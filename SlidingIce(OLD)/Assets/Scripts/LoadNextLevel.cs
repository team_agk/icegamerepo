﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour {

	[SerializeField]
	private string nextLevelName;

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "PlayerFace")
		SceneManager.LoadScene (nextLevelName);
	}
}
